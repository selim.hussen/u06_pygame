from multiprocessing import connection
import pygame, sys 
from settings import *
import sqlite3

import sqlite3

try:
	
	# Connect to DB and create a cursor
	sqliteConnection = sqlite3.connect('database.db')
	cursor = sqliteConnection.cursor()
	print('DB Init')

	# Write a query and execute it with cursor
	query = 'select sqlite_version();'
	cursor.execute(query)

	# Fetch and output result
	result = cursor.fetchall()
	print('SQLite Version is {}'.format(result))

	# Close the cursor
	cursor.close()

# Handle errors
except sqlite3.Error as error:
	print('Error occured - ', error)

# Close DB Connection irrespective of success
# or failure
finally:
	
	if sqliteConnection:
		sqliteConnection.close()
		print('SQLite Connection closed')



class Game: 
    def __init__(self):

       # general setup
       pygame.init()
       self.screen = pygame.display.set_mode((WIDTH,HEIGTH))
       pygame.display.set_caption('Velo')
       self.clock = pygame.time.Clock()

    def run(self):
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()

            self.screen.fill('black')
            pygame.display.update()
            self.clock.tick(FPS) 

if __name__ == '__main__':
    game = Game()
    game.run()                      





